#ifndef DUNE_ELASTICITY_MATERIALS_HENCKYDENSITY_HH
#define DUNE_ELASTICITY_MATERIALS_HENCKYDENSITY_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fmatrixev.hh>

#include <dune/elasticity/materials/localdensity.hh>

namespace Dune::Elasticity {

template<int dim, class field_type = double, class ctype = double>
class HenckyDensity final
  : public LocalDensity<dim,field_type>
{
public:
  /** \brief Constructor with a set of material parameters
    *
    * \param parameters The material parameters
    */
  HenckyDensity(const Dune::ParameterTree& parameters)
  {
    // Lame constants
    mu_ = parameters.template get<double>("mu");
    lambda_ = parameters.template get<double>("lambda");
    kappa_  = (2.0 * mu_ + 3.0 * lambda_) / 3.0;
  }

  /** \brief Evaluation with the deformation gradient
  *
  * \param x Position of the gradient
  * \param gradient Deformation gradient
  */
  field_type operator() (const FieldVector<ctype,dim>& x, const FieldMatrix<field_type,dim,dim>& gradient) const
  {
    /////////////////////////////////////////////////////////
    // compute C = F^TF
    /////////////////////////////////////////////////////////

    Dune::FieldMatrix<field_type,dim,dim> C(0);
    for (int i=0; i<dim; i++)
      for (int j=0; j<dim; j++)
        for (int k=0; k<dim; k++)
          C[i][j] += gradient[k][i] * gradient[k][j];

    //////////////////////////////////////////////////////////
    //  Eigenvalues of C = F^TF
    //////////////////////////////////////////////////////////

    Dune::FieldVector<field_type,dim> sigmaSquared;
    FMatrixHelp::eigenValues(C, sigmaSquared);

    // logarithms of the singular values of F, i.e., eigenvalues of U
    std::array<field_type, dim> lnSigma;
    for (int i = 0; i < dim; i++)
      lnSigma[i] = 0.5 * log(sigmaSquared[i]);

    field_type trLogU = 0;
    for (int i = 0; i < dim; i++)
      trLogU += lnSigma[i];

    field_type normDevLogUSquared = 0;
    for (int i = 0; i < dim; i++)
    {
      // the deviator shifts the spectrum by the trace
      field_type lnDevSigma_i = lnSigma[i] - (1.0 / double(dim)) * trLogU;
      normDevLogUSquared += lnDevSigma_i * lnDevSigma_i;
    }

    // Add the local energy density
    auto distortionEnergy = mu_ * normDevLogUSquared;
    auto dilatationEnergy = 0.5 * kappa_ * (trLogU * trLogU);

    return dilatationEnergy + distortionEnergy;
  }

  /** \brief Lame constants */
  double mu_, lambda_, kappa_;
};

} // namespace Dune::Elasticity

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_HENCKYDENSITY_HH


