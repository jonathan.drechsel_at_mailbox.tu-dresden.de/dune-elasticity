#ifndef DUNE_ELASTICITY_MATERIALS_STVENANTKIRCHHOFFDENSITY_HH
#define DUNE_ELASTICITY_MATERIALS_STVENANTKIRCHHOFFDENSITY_HH

#include <dune/common/fmatrix.hh>

#include <dune/elasticity/materials/localdensity.hh>

namespace Dune::Elasticity {

template<int dim, class field_type = double, class ctype = double>
class StVenantKirchhoffDensity final
  : public LocalDensity<dim,field_type>
{
public:

  /** \brief Constructor with a set of material parameters
    * \param parameters The material parameters
    */
  StVenantKirchhoffDensity(const Dune::ParameterTree& parameters)
  {
    // Lame constants
    mu_ = parameters.template get<double>("mu");
    lambda_ = parameters.template get<double>("lambda");
  }

  /** \brief Evaluation with the deformation gradient
  *
  * \param x Position of the gradient
  * \param gradient Deformation gradient
  */
  field_type operator() (const FieldVector<ctype,dim>& x, const FieldMatrix<field_type,dim,dim>& gradient) const
  {
    //////////////////////////////////////////////////////////
    // compute strain E = 1/2 *( F^T F - I)
    /////////////////////////////////////////////////////////

    Dune::FieldMatrix<field_type,dim,dim> FTF(0);
    for (int i=0; i<dim; i++)
      for (int j=0; j<dim; j++)
        for (int k=0; k<dim; k++)
          FTF[i][j] += gradient[k][i] * gradient[k][j];

    Dune::FieldMatrix<field_type,dim,dim> E = FTF;
    for (int i=0; i<dim; i++)
      E[i][i] -= 1.0;
    E *= 0.5;

    /////////////////////////////////////////////////////////
    //  Compute energy
    /////////////////////////////////////////////////////////

    field_type trE = field_type(0);
    for (int i=0; i<dim; i++)
      trE += E[i][i];

    // The trace of E^2, happens to be its squared Frobenius norm
    field_type trESquared = E.frobenius_norm2();

    return mu_ * trESquared + 0.5 * lambda_ * trE * trE;
  }

  /** \brief Lame constants */
  double mu_, lambda_;
};

} // namespace Dune::Elasticity

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_STVENANTKIRCHHOFFDENSITY_HH
