#ifndef DUNE_ELASTICITY_MATERIALS_NEOHOOKEENERGY_HH
#define DUNE_ELASTICITY_MATERIALS_NEOHOOKEENERGY_HH

#include <dune/elasticity/materials/neohookedensity.hh>
#include <dune/elasticity/materials/localintegralenergy.hh>

namespace Dune {

template<class GridView, class LocalFiniteElement, class field_type=double>
class DUNE_DEPRECATED_MSG("Use Elasticity::LocalIntegralEnergy with Elasticity::NeoHookeDensity")
NeoHookeEnergy
  : public Elasticity::LocalIntegralEnergy<GridView,LocalFiniteElement,field_type>
{

  using Base = Elasticity::LocalIntegralEnergy<GridView,LocalFiniteElement,field_type>;
public:

  // backwards compatibility: forward directly to integral energy
  NeoHookeEnergy(const Dune::ParameterTree& parameters)
  : Base(std::make_shared<Elasticity::NeoHookeDensity<GridView::dimension,field_type>>(parameters))
  {}
};

}  // namespace Dune

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_NEOHOOKEENERGY_HH
