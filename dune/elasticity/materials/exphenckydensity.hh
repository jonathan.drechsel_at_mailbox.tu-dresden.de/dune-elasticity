#ifndef DUNE_ELASTICITY_MATERIALS_EXPHENCKYDENSITY_HH
#define DUNE_ELASTICITY_MATERIALS_EXPHENCKYDENSITY_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fmatrixev.hh>

#include <dune/elasticity/materials/localdensity.hh>

namespace Dune::Elasticity {

template<int dim, class field_type = double, class ctype = double>
class ExpHenckyDensity final
  : public LocalDensity<dim,field_type>
{
public:

  /** \brief Constructor with a set of material parameters
  * \param parameters The material parameters
  */
  ExpHenckyDensity(const Dune::ParameterTree& parameters)
  {
    // Lame constants
    mu_     = parameters.get<double>("mu");
    lambda_ = parameters.get<double>("lambda");
    kappa_  = (2.0 * mu_ + 3.0 * lambda_) / 3.0;

    // Exponential Hencky constants with naming convention from Neff, Ghiba and Lankeit,
    // "The exponentiated Hencky-logarithmic strain energy. Part I: Constitutive issues
    //  and rank one convexity"
    k_      = parameters.get<double>("k");
    khat_   = parameters.get<double>("khat");

    // weights for distortion and dilatation parts of the energy
    alpha_  = mu_ / k_;
    beta_   = kappa_ / (2.0 * khat_);
  }

  /** \brief Evaluation with the deformation gradient
  *
  * \param x Position of the gradient
  * \param gradient Deformation gradient
  */
  field_type operator() (const FieldVector<ctype,dim>& x, const FieldMatrix<field_type,dim,dim>& gradient) const
  {
    /////////////////////////////////////////////////////////
    // compute C = F^TF
    /////////////////////////////////////////////////////////

    Dune::FieldMatrix<field_type,dim,dim> C(0);
    for (int i=0; i<dim; i++)
      for (int j=0; j<dim; j++)
        for (int k=0; k<dim; k++)
          C[i][j] += gradient[k][i] * gradient[k][j];

    //////////////////////////////////////////////////////////
    //  Eigenvalues of FTF
    //////////////////////////////////////////////////////////

    // compute eigenvalues of C, i.e., squared singular values \sigma_i of F
    Dune::FieldVector<field_type, dim> sigmaSquared;
    FMatrixHelp::eigenValues(C, sigmaSquared);

    // logarithms of the singular values of F, i.e., eigenvalues of U
    std::array<field_type, dim> lnSigma;
    for (int i = 0; i < dim; i++)
      lnSigma[i] = 0.5 * log(sigmaSquared[i]);

    field_type trLogU = 0;
    for (int i = 0; i < dim; i++)
      trLogU += lnSigma[i];

    field_type normDevLogUSquared = 0;
    for (int i = 0; i < dim; i++)
    {
      // the deviator shifts the
      field_type lnDevSigma_i = lnSigma[i] - (1.0 / double(dim)) * trLogU;
      normDevLogUSquared += lnDevSigma_i * lnDevSigma_i;
    }

    // Add the local energy density
    auto distortionEnergy = alpha_ * exp(khat_ * normDevLogUSquared);
    auto dilatationEnergy = beta_  * exp(k_ * (trLogU * trLogU));

    return distortionEnergy + dilatationEnergy;
  }

  /** \brief Lame constants */
  field_type mu_, lambda_, kappa_, k_, khat_, alpha_, beta_;
};

} // namespace Dune::Elasticity

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_EXPHENCKYENERGYDENSITY_HH


