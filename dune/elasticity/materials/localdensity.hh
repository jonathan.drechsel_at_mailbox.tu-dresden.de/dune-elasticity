#ifndef DUNE_ELASTICITY_MATERIALS_LOCALDENSITY_HH
#define DUNE_ELASTICITY_MATERIALS_LOCALDENSITY_HH

#include <dune/common/fmatrix.hh>

namespace Dune::Elasticity {

/** \brief A base class for energy densities to be evaluated in an integral energy
 *
 * \tparam field_type type of the gradient entries
 * \tparam ctype type of the coordinates, i.e., the x entries
 */
template<int dim, class field_type = double, class ctype = double>
class LocalDensity
{

public:

  /** \brief Evaluation with the deformation gradient
   *
   * \param x Position of the gradient
   * \param gradient Deformation gradient
   */
  virtual field_type operator() (const FieldVector<ctype,dim>& x, const FieldMatrix<field_type,dim,dim>& gradient) const = 0;

};

}  // namespace Dune::Elasticity

#endif  // DUNE_ELASTICITY_MATERIALS_LOCALDENSITY_HH
