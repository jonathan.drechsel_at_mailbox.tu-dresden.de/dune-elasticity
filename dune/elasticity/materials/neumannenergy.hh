#ifndef DUNE_ELASTICITY_MATERIALS_NEUMANNENERGY_HH
#define DUNE_ELASTICITY_MATERIALS_NEUMANNENERGY_HH

#include <functional>

#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/elasticity/assemblers/localenergy.hh>

namespace Dune::Elasticity {

template<class LocalView, class field_type=double>
class NeumannEnergy
  : public Elasticity::LocalEnergy<LocalView, field_type>
{
  using GridView = typename LocalView::GridView;
  using DT = typename GridView::Grid::ctype;
  using ctype = typename GridView::ctype;

  enum {dim=GridView::dimension};

public:

  /** \brief Constructor with a set of material parameters
   * \param parameters The material parameters
   */
  NeumannEnergy(const std::shared_ptr<BoundaryPatch<GridView>>& neumannBoundary,
                std::function<Dune::FieldVector<double,dim>(Dune::FieldVector<ctype,dim>)> neumannFunction)
  : neumannBoundary_(neumannBoundary),
    neumannFunction_(neumannFunction)
  {}

  /** \brief Assemble the energy for a single element */
  field_type energy (const LocalView& localView,
                     const std::vector<field_type>& localConfiguration) const
  {
    const auto& localFiniteElement = localView.tree().child(0).finiteElement();
    const auto& element = localView.element();

    field_type energy = 0;

    for (auto&& it : intersections(neumannBoundary_->gridView(), element)) {

      if (not neumannBoundary_ or not neumannBoundary_->contains(it))
        continue;

      int quadOrder = localFiniteElement.localBasis().order();

      const auto& quad = Dune::QuadratureRules<ctype, dim-1>::rule(it.type(), quadOrder);

      for (size_t pt=0; pt<quad.size(); pt++) {

        // Local position of the quadrature point
        const Dune::FieldVector<ctype,dim>& quadPos = it.geometryInInside().global(quad[pt].position());

        const auto integrationElement = it.geometry().integrationElement(quad[pt].position());

        // The value of the local function
        std::vector<Dune::FieldVector<ctype,1> > shapeFunctionValues;
        localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);

        Dune::FieldVector<field_type,dim> value(0);
        for (size_t i=0; i<localFiniteElement.size(); i++)
          for (int j=0; j<dim; j++)
            value[j] += shapeFunctionValues[i] * localConfiguration[ localView.tree().child(j).localIndex(i) ];

        // Value of the Neumann data at the current position
        auto neumannValue = neumannFunction_( it.geometry().global(quad[pt].position()) );

        // Only translational dofs are affected by the Neumann force
        for (size_t i=0; i<neumannValue.size(); i++)
          energy += (neumannValue[i] * value[i]) * quad[pt].weight() * integrationElement;

      }

    }

    return energy;
  }

private:
  /** \brief The Neumann boundary */
  const std::shared_ptr<BoundaryPatch<GridView>> neumannBoundary_;

  /** \brief The function implementing the Neumann data */
  std::function<Dune::FieldVector<double,dim>(Dune::FieldVector<ctype,dim>)> neumannFunction_;
};

}  // namespace Dune::Elasticity


namespace Dune {

template<class GridView, class LocalFiniteElement, class field_type=double>
class
NeumannEnergy
: public LocalEnergy<GridView,LocalFiniteElement,std::vector<FieldVector<field_type,GridView::dimension> > >
{
 // grid types
  typedef typename GridView::ctype ctype;
  typedef typename GridView::template Codim<0>::Entity Entity;

  enum {dim=GridView::dimension};

public:

  /** \brief Constructor with a set of material parameters
   * \param parameters The material parameters
   */
  NeumannEnergy(const std::shared_ptr<BoundaryPatch<GridView>>& neumannBoundary,
                const std::shared_ptr<std::function<Dune::FieldVector<double,dim>(Dune::FieldVector<ctype,dim>)>>& neumannFunction)
  : neumannBoundary_(neumannBoundary),
    neumannFunction_(neumannFunction)
  {}

  /** \brief Assemble the energy for a single element */
  field_type energy (const Entity& element,
                     const LocalFiniteElement& localFiniteElement,
                     const std::vector<Dune::FieldVector<field_type,dim> >& localConfiguration) const
  {
    assert(element.type() == localFiniteElement.type());

    field_type energy = 0;

    for (auto&& it : intersections(neumannBoundary_->gridView(), element)) {

      if (not neumannBoundary_ or not neumannBoundary_->contains(it))
        continue;

      int quadOrder = localFiniteElement.localBasis().order();

      const auto& quad = Dune::QuadratureRules<ctype, dim-1>::rule(it.type(), quadOrder);

      for (size_t pt=0; pt<quad.size(); pt++) {

        // Local position of the quadrature point
        const Dune::FieldVector<ctype,dim>& quadPos = it.geometryInInside().global(quad[pt].position());

        const auto integrationElement = it.geometry().integrationElement(quad[pt].position());

        // The value of the local function
        std::vector<Dune::FieldVector<ctype,1> > shapeFunctionValues;
        localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);

        Dune::FieldVector<field_type,dim> value(0);
        for (size_t i=0; i<localFiniteElement.size(); i++)
          for (int j=0; j<dim; j++)
            value[j] += shapeFunctionValues[i] * localConfiguration[i][j];

        // Value of the Neumann data at the current position
        auto neumannValue = (*neumannFunction_)( it.geometry().global(quad[pt].position()) );


        // Only translational dofs are affected by the Neumann force
        for (size_t i=0; i<neumannValue.size(); i++)
          energy += (neumannValue[i] * value[i]) * quad[pt].weight() * integrationElement;

      }

    }

    return energy;
  }

private:
  /** \brief The Neumann boundary */
  const std::shared_ptr<BoundaryPatch<GridView>>
    DUNE_DEPRECATED_MSG("Use dune-functions powerBases with LocalView concept. See Elasticity::NeumannEnergy") neumannBoundary_;

  /** \brief The function implementing the Neumann data */
  const std::shared_ptr<std::function<Dune::FieldVector<double,dim>(Dune::FieldVector<ctype,dim>)>> neumannFunction_;
};

}  // namespace Dune


#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_NEUMANNENERGY_HH
