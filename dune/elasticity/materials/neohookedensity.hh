#ifndef DUNE_ELASTICITY_MATERIALS_NEOHOOKEDENSITY_HH
#define DUNE_ELASTICITY_MATERIALS_NEOHOOKEDENSITY_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fmatrixev.hh>

#include <dune/elasticity/materials/localdensity.hh>

namespace Dune::Elasticity {

template<int dim, class field_type = double, class ctype = double>
class NeoHookeDensity final
  : public LocalDensity<dim,field_type>
{
public:

  /** \brief Constructor with a set of material parameters
  * \param parameters The material parameters
  */
  NeoHookeDensity(const Dune::ParameterTree& parameters)
  {
      // Lame constants
      mu_     = parameters.get<double>("mu");
      lambda_ = parameters.get<double>("lambda");
      kappa_  = (2.0 * mu_ + 3.0 * lambda_) / 3.0;
  }

  /** \brief Evaluation with the deformation gradient
  *
  * \param x Position of the gradient
  * \param gradient Deformation gradient
  */
  field_type operator() (const FieldVector<ctype,dim>& x, const FieldMatrix<field_type,dim,dim>& gradient) const
  {
    /////////////////////////////////////////////////////////
    // compute C = F^TF
    /////////////////////////////////////////////////////////

    Dune::FieldMatrix<field_type,dim,dim> C(0);
    for (int i=0; i<dim; i++)
      for (int j=0; j<dim; j++)
        for (int k=0; k<dim; k++)
          C[i][j] += gradient[k][i] * gradient[k][j];

    //////////////////////////////////////////////////////////
    //  Eigenvalues of FTF
    //////////////////////////////////////////////////////////

    // eigenvalues of C, i.e., squared singular values \sigma_i of F
    Dune::FieldVector<field_type, dim> sigmaSquared;
    FMatrixHelp::eigenValues(C, sigmaSquared);

    using std::sqrt;

    // singular values of F, i.e., eigenvalues of U
    std::array<field_type, dim> sigma;
    for (int i = 0; i < dim; i++)
      sigma[i] = sqrt(sigmaSquared[i]);

    field_type detC = 1.0;
    for (int i = 0; i < dim; i++)
      detC *= sigmaSquared[i];
    field_type detF = sqrt(detC);

    // \tilde{C} = \tilde{F}^T\tilde{F} = \frac{1}{\det{F}^{2/3}}C
    field_type trCTilde = 0;
    for (int i = 0; i < dim; i++)
      trCTilde += sigmaSquared[i];
    trCTilde /= pow(detC, 1.0/3.0);

    // Add the local energy density
    auto distortionEnergy = (0.5 * mu_)    * (trCTilde - 3);
    auto dilatationEnergy = (0.5 * kappa_) * ((detF - 1) * (detF - 1));

    return distortionEnergy + dilatationEnergy;
  }

  /** \brief Lame constants */
  field_type mu_, lambda_, kappa_;
};

} // namespace Dune::Elasticity

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_NEOHOOKEDENSITY_HH
