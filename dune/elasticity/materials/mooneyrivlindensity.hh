#ifndef DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINDENSITY_HH
#define DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINDENSITY_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fmatrixev.hh>

#include <dune/elasticity/materials/localdensity.hh>

namespace Dune::Elasticity {

template<int dim, class field_type = double, class ctype = double>
class MooneyRivlinDensity final
    : public LocalDensity<dim,field_type>
{
public:

  /** \brief Constructor with a set of material parameters
  * \param parameters The material parameters
  */
  MooneyRivlinDensity(const Dune::ParameterTree& parameters)
  {
    mooneyrivlin_energy = parameters.get<std::string>("mooneyrivlin_energy");
    // Mooneyrivlin constants
    if (mooneyrivlin_energy == "ciarlet") {
      mooneyrivlin_a = parameters.get<double>("mooneyrivlin_a");
      mooneyrivlin_b = parameters.get<double>("mooneyrivlin_b");
      mooneyrivlin_c = parameters.get<double>("mooneyrivlin_c");
    } else if (mooneyrivlin_energy == "log" or mooneyrivlin_energy == "square") {
      mooneyrivlin_10 = parameters.get<double>("mooneyrivlin_10");
      mooneyrivlin_01 = parameters.get<double>("mooneyrivlin_01");
      mooneyrivlin_20 = parameters.get<double>("mooneyrivlin_20");
      mooneyrivlin_02 = parameters.get<double>("mooneyrivlin_02");
      mooneyrivlin_11 = parameters.get<double>("mooneyrivlin_11");
      mooneyrivlin_30 = parameters.get<double>("mooneyrivlin_30");
      mooneyrivlin_03 = parameters.get<double>("mooneyrivlin_03");
      mooneyrivlin_21 = parameters.get<double>("mooneyrivlin_21");
      mooneyrivlin_12 = parameters.get<double>("mooneyrivlin_12");
      mooneyrivlin_k = parameters.get<double>("mooneyrivlin_k");
    } else {
      DUNE_THROW(Exception, "Error: Selected mooneyrivlin implementation (" << mooneyrivlin_energy << ") not available!");
    }
  }

  /** \brief Evaluation with the deformation gradient
  *
  * \param x Position of the gradient
  * \param gradient Deformation gradient
  */
  field_type operator() (const FieldVector<ctype,dim>& x, const FieldMatrix<field_type,dim,dim>& gradient) const
  {
    /////////////////////////////////////////////////////////
    // compute C = F^TF
    /////////////////////////////////////////////////////////

    Dune::FieldMatrix<field_type,dim,dim> C(0);
    for (int i=0; i<dim; i++)
      for (int j=0; j<dim; j++)
        for (int k=0; k<dim; k++)
          C[i][j] += gradient[k][i] * gradient[k][j];

    //////////////////////////////////////////////////////////
    //  Eigenvalues of C = F^TF
    //////////////////////////////////////////////////////////


    // eigenvalues of C, i.e., squared singular values \sigma_i of F
    Dune::FieldVector<field_type, dim> sigmaSquared;
    FMatrixHelp::eigenValues(C, sigmaSquared);

    field_type normFSquared = gradient.frobenius_norm2();
    field_type detF = gradient.determinant();

    if (detF < 0)
      DUNE_THROW( Dune::Exception, "det(F) < 0, so it is not possible to calculate the MooneyRivlinEnergy.");

    field_type normFinvSquared = 0;

    field_type c2Tilde = 0;
    for (int i = 0; i < dim; i++) {
      normFinvSquared += 1/sigmaSquared[i];
      // compute D, which is the sum of the squared eigenvalues
      for (int j = i+1; j < dim; j++)
        c2Tilde += sigmaSquared[j]*sigmaSquared[i];
      }

    field_type trCTildeMinus3 = normFSquared/pow(detF, 2.0/dim) - 3;
    // \tilde{D} = \frac{1}{\det{F}^{4/3}}D -> divide by det(F)^(4/3)= det(C)^(2/3)
    c2Tilde /= pow(detF, 4.0/dim);
    field_type c2TildeMinus3 = c2Tilde - 3;

    // Add the local energy density
    field_type strainEnergy = 0;

    if (mooneyrivlin_energy == "ciarlet")
    {
      using std::log;
      return mooneyrivlin_a*normFSquared + mooneyrivlin_b*normFinvSquared*detF + mooneyrivlin_c*detF*detF - ((dim-1)*mooneyrivlin_a + mooneyrivlin_b + 2*mooneyrivlin_c)*log(detF);
    } else {
      strainEnergy = mooneyrivlin_10 * trCTildeMinus3 +
                        mooneyrivlin_01 * c2TildeMinus3  +
                        mooneyrivlin_20 * trCTildeMinus3 * trCTildeMinus3 +
                        mooneyrivlin_02 * c2TildeMinus3  * c2TildeMinus3  +
                        mooneyrivlin_11 * trCTildeMinus3 * c2TildeMinus3  +
                        mooneyrivlin_30 * trCTildeMinus3 * trCTildeMinus3 * trCTildeMinus3 +
                        mooneyrivlin_21 * trCTildeMinus3 * trCTildeMinus3 * c2TildeMinus3  +
                        mooneyrivlin_12 * trCTildeMinus3 * c2TildeMinus3  * c2TildeMinus3  +
                        mooneyrivlin_03 * c2TildeMinus3  * c2TildeMinus3  * c2TildeMinus3;
      if (mooneyrivlin_energy == "log") {
        using std::log;
        field_type logDetF = log(detF);
        return strainEnergy + 0.5 * mooneyrivlin_k* logDetF * logDetF;
      } else { //mooneyrivlin_energy is "square"
        field_type detFMinus1 = detF - 1;
        return strainEnergy + mooneyrivlin_k* detFMinus1 * detFMinus1;
      }
    }
  }

  /** \brief Lame constants */
  field_type mooneyrivlin_a,
             mooneyrivlin_b,
             mooneyrivlin_c,
             mooneyrivlin_10,
             mooneyrivlin_01,
             mooneyrivlin_20,
             mooneyrivlin_02,
             mooneyrivlin_11,
             mooneyrivlin_30,
             mooneyrivlin_21,
             mooneyrivlin_12,
             mooneyrivlin_03,
             mooneyrivlin_k;
  std::string mooneyrivlin_energy;
};

} // namespace Dune::Elasticity

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINDENSITY_HH
