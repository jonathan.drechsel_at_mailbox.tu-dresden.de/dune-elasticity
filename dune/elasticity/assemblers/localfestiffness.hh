#ifndef LOCAL_FE_STIFFNESS_HH
#define LOCAL_FE_STIFFNESS_HH

#include <dune/common/fmatrix.hh>
#include <dune/istl/matrix.hh>

#include <dune/elasticity/assemblers/localenergy.hh>

namespace Dune::Elasticity {

template<class LocalView, class RT = double>
class LocalFEStiffness
: public Dune::Elasticity::LocalEnergy<LocalView, RT>
{
    enum {gridDim=LocalView::GridView::dimension};

public:

    /** \brief Assemble the local gradient and tangent matrix at the current position
    */
    virtual void assembleGradientAndHessian(const LocalView& localView,
                                            const std::vector<RT>& localConfiguration,
                                            std::vector<RT>& localGradient,
                                            Dune::Matrix<RT>& localHessian
                                           ) = 0;

};

} // namespace Dune::Elasticity


template<class GridView, class LocalFiniteElement, class VectorType>
class
LocalFEStiffness
: public Dune::LocalEnergy<GridView, LocalFiniteElement, VectorType>
{
    // grid types
    typedef typename GridView::Grid::ctype DT;
    typedef typename VectorType::value_type::field_type RT;
    typedef typename GridView::template Codim<0>::Entity Entity;

    // some other sizes
    enum {gridDim=GridView::dimension};

public:

    //! Dimension of a tangent space
    enum { blocksize = VectorType::value_type::dimension };

    /** \brief Assemble the local gradient and tangent matrix at the current position
    */
    virtual void assembleGradientAndHessian(const Entity& e,
                                 const LocalFiniteElement& localFiniteElement,
                                 const VectorType& localConfiguration,
                                 VectorType& localGradient) = 0;

    // assembled data
    Dune::Matrix<Dune::FieldMatrix<RT,blocksize,blocksize> >
      DUNE_DEPRECATED_MSG("Use dune-functions powerBases with LocalView concept. See Dune::Elasticity::LocalFEStiffness") A_;

};

#endif

