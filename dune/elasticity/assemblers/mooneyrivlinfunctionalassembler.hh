#ifndef MOONEY_RIVLIN_FUNCTIONAL_ASSEMBLER_HH
#define MOONEY_RIVLIN_FUNCTIONAL_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include "dune/fufem/assemblers/localfunctionalassembler.hh"
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

#include <dune/elasticity/common/elasticityhelpers.hh>
#include <dune/matrix-vector/addtodiagonal.hh>

/** \brief Local assembler for the linearization of a nonlinear Mooney-Rivlin energy at a displacement u
 *
 *    \f[
 *      W(u)= a tr(E(u)) + b tr(E(u))^2 + c ||E(u)||^2 + d J^2 + e J^-k,  k>=2
 *    \f]
 *
 *   where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$\a\f$,...,\f$\e\f$ material parameters
 *      - \f$k\f$ controlls orientation preservation (k \approx \floor(1/(1-2nu) -1))
*/
template <class GridType, class TrialLocalFE>
class MooneyRivlinFunctionalAssembler :
    public LocalFunctionalAssembler<GridType,TrialLocalFE, Dune::FieldVector<typename GridType::ctype ,GridType::dimension> >

{
    static constexpr int dim = GridType::dimension;
    using ctype = typename GridType::ctype;
    using T = Dune::FieldVector<ctype, dim>;
    using Base = LocalFunctionalAssembler<GridType,TrialLocalFE, T>;

public:
    using typename Base::Element;
    using typename Base::LocalVector;
    using GridFunction = VirtualGridFunction<GridType, T >;

    MooneyRivlinFunctionalAssembler() = default;

    //! Create assembler from material parameters
    MooneyRivlinFunctionalAssembler(ctype a, ctype b, ctype c, ctype d, ctype e, int k) :
        a_(a),b_(b),c_(c),d_(d),e_(e),k_(k)
    {}

    void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
    {
        using FMatrix = Dune::FieldMatrix<ctype,dim,dim>;
        using JacobianType = typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType;

        int rows = tFE.localBasis().size();

        localVector.resize(rows);
        localVector = 0.0;

        // get quadrature rule (should depend on k?)
        const int order = (element.type().isSimplex())
            ? 6*(tFE.localBasis().order())
            : 6*(tFE.localBasis().order()*dim);
        const auto& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
        std::vector<T> gradients(tFE.localBasis().size());

        const auto& geometry = element.geometry();

        for (const auto& pt : quad) {

            const auto& quadPos = pt.position();
            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);
            const ctype integrationElement = geometry.integrationElement(quadPos);

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute transformed gradients of base functions
            for (size_t i=0; i < gradients.size(); ++i)
                invJacobian.mv(referenceGradients[i][0], gradients[i]);

            // evaluate the displacement gradient at the quadrature point
            typename GridFunction::DerivativeType localConfGrad;
            if (displacement_->isDefinedOn(element))
                displacement_->evaluateDerivativeLocal(element, quadPos, localConfGrad);
            else
                displacement_->evaluateDerivative(geometry.global(quadPos),localConfGrad);

            // compute linearization of the determinante of the deformation gradient
            auto linDefDet = Dune::Elasticity::linearisedDefDet(localConfGrad);
            auto strain = Dune::Elasticity::strain(localConfGrad);

            // make deformation gradient out of the discplacement
            Dune::MatrixVector::addToDiagonal(localConfGrad, 1.0);

            const auto J = localConfGrad.determinant();

            //compute linearized strain for all shapefunctions
            std::vector<std::array<SymmetricTensor<dim>, dim> > linStrain(rows);

            for (int i=0; i < rows; i++)
                for (int k=0; k < dim; k++) {

                    // The deformation gradient of the shape function
                    FMatrix deformationGradient(0);
                    deformationGradient[k] = gradients[i];

                    // The linearized strain is the symmetric product of defGrad and (Id+localConf)
                    linStrain[i][k] = Dune::Elasticity::linearisedStrain(deformationGradient, localConfGrad);
                }

            // collect terms
            FMatrix fu(0);

            // the linearization of the trace is just deformation gradient
            fu.axpy(a_+2*b_*strain.trace(), localConfGrad);

            // linearization of the compressibility function
            fu.axpy(2*d_*J - e_*k_*std::pow(J,-k_-1), linDefDet);

            ctype z = pt.weight()*integrationElement;

            // add vector entries
            for(int row=0; row < rows; row++) {
                fu.usmv(z, gradients[row], localVector[row]);

                // linearization of the tensor contraction
                for (int rcomp=0; rcomp < dim; rcomp++)
                    localVector[row][rcomp] +=(strain*linStrain[row][rcomp])*z*2*c_;
            }
        }
    }

    /** \brief Set new configuration. In Newton iterations this needs to be assembled more than one time.*/
    void setConfiguration(std::shared_ptr<GridFunction> newDisplacement) {
        displacement_ = newDisplacement;
    }

private:
    //! Material parameters
    ctype a_; ctype b_; ctype c_; ctype d_; ctype e_;
    //! Compressibility
    int k_;

    /** \brief The configuration at which the functional is evaluated.*/
    std::shared_ptr<GridFunction> displacement_;
};

#endif


