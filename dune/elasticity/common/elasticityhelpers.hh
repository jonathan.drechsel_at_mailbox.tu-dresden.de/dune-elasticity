#ifndef DUNE_ELASTICITY_HELPERS_HH
#define DUNE_ELASTICITY_HELPERS_HH

#include <cmath>

#include <dune/common/fmatrix.hh>

#include <dune/fufem/symmetrictensor.hh>

namespace Dune {

    namespace Elasticity {

        /** \brief Compute nonlinear strain tensor from a displacement gradient.
         *
         *  \param grad The gradient of the direction in which the linearisation is computed.
         *  \param strain The tensor to store the strain in.
         */
        template <int dim, class field_type=double>
        void strain(const Dune::FieldMatrix<field_type, dim, dim>& grad, SymmetricTensor<dim,field_type>& E) {
            E = field_type(0);
            for (int i=0; i<dim ; ++i) {
                E(i,i) +=grad[i][i];

                for (int k=0;k<dim;++k)
                    E(i,i) += 0.5*grad[k][i]*grad[k][i];

                for (int j=i+1; j<dim; ++j) {
                    E(i,j) += 0.5*(grad[i][j] + grad[j][i]);
                    for (int k=0;k<dim;++k)
                        E(i,j) += 0.5*grad[k][i]*grad[k][j];
                }
            }
        }

        /** \brief Compute nonlinear strain tensor from a displacement gradient.
         *
         *  \param grad The gradient of the direction in which the linearisation is computed.
         */
        template <int dim, class field_type=double>
        auto strain(const Dune::FieldMatrix<field_type, dim, dim>& grad) {
            SymmetricTensor<dim, field_type> E{field_type{0}};
            for (int i=0; i<dim ; ++i) {
                E(i,i) += grad[i][i];

                for (int k=0;k<dim;++k)
                    E(i,i) += 0.5*grad[k][i]*grad[k][i];

                for (int j=i+1; j<dim; ++j) {
                    E(i,j) += 0.5*(grad[i][j] + grad[j][i]);
                    for (int k=0;k<dim;++k)
                        E(i,j) += 0.5*grad[k][i]*grad[k][j];
                }
            }
            return E;
        }


        /** \brief Compute linearised strain at the identity from a given displacement gradient.
         *
         *  \param grad The gradient of the direction in which the linearisation is computed.
         *  \param strain The tensor to store the strain in.
         */
        template <int dim, class field_type=double>
        static void linearisedStrain(const Dune::FieldMatrix<field_type, dim, dim>& grad, SymmetricTensor<dim,field_type>& strain) {
            for (int i=0; i<dim ; ++i)
            {
                strain(i,i) = grad[i][i];
                for (int j=i+1; j<dim; ++j)
                    strain(i,j) = 0.5*(grad[i][j] + grad[j][i]);
            }
        }

        /** \brief Compute linearised strain at the identity from a given displacement gradient.
         *
         *  \param grad The gradient of the direction in which the linearisation is computed.
         */
        template <int dim, class field_type=double>
        auto linearisedStrain(const Dune::FieldMatrix<field_type, dim, dim>& grad) {
            SymmetricTensor<dim, field_type> strain{field_type{0}};

            for (int i=0; i<dim ; ++i) {
                strain(i,i) = grad[i][i];
                for (int j=i+1; j<dim; ++j)
                    strain(i,j) = 0.5*(grad[i][j] + grad[j][i]);
            }
            return strain;
        }


        /** \brief Compute linearised strain at a prescribed configuration in the direction of a given displacement gradient.
         *
         *  \param grad The gradient of the direction in which the linearisation is computed.
         *  \param conf The deformation gradient(!) of the configuration at which the linearisation is evaluated.
         *  \param strain The tensor to store the strain in.
         */
        template <int dim, class field_type=double>
        void linearisedStrain(const Dune::FieldMatrix<field_type, dim, dim>& grad, const Dune::FieldMatrix<field_type, dim, dim>& conf,
                                     SymmetricTensor<dim,field_type>& strain) {
            strain = 0;
            for (int i=0;i<dim; i++)
                for (int j=i;j<dim;j++)
                    for (int k=0;k<dim;k++)
                        strain(i,j)+= grad[k][i]*conf[k][j]+conf[k][i]*grad[k][j];
            strain *= 0.5;
        }

        /** \brief Compute linearised strain at a prescribed configuration in the direction of a given displacement gradient.
         *
         *  \param grad The gradient of the direction in which the linearisation is computed.
         *  \param conf The deformation gradient(!) of the configuration at which the linearisation is evaluated.
         */
        template <int dim, class field_type=double>
        auto linearisedStrain(const Dune::FieldMatrix<field_type, dim, dim>& grad, const Dune::FieldMatrix<field_type, dim, dim>& conf)
        {
            SymmetricTensor<dim, field_type> strain{field_type{0}};
            for (int i = 0; i < dim; i++)
                for (int j = i; j < dim; j++)
                    for (int k = 0; k < dim; k++)
                        strain(i,j)+= grad[k][i]*conf[k][j] + conf[k][i]*grad[k][j];
            strain *= 0.5;
            return strain;
        }


        /** \brief The ogden material penalty term \f$ \Gamma \f$ */
        template <class field_type=double>
        static field_type Gamma(field_type x) {
            return x*x- std::log(x);
        }

        /** \brief First derivative of the ogden penalty term \f$ \Gamma \f$ */
        template <class field_type=double>
        static inline field_type Gamma_x(field_type x) {
            return 2*x - 1/x;
        }

        /** \brief Second derivative of the ogden penalty term \f$ \Gamma \f$ */
        template <class field_type=double>
        static inline field_type Gamma_xx(field_type x) {
            return 2 + 1/(x*x);
        }

        // The Kronecker delta function
        template <int i, int j>
        struct Delta {
            static int const delta = 0;
        };

        template <int i>
        struct Delta<i,i> {
            static int const delta = 1;
        };

        template <int i, int j, int p, int q, int dim>
        static inline double E_du(const FieldMatrix<double,dim,dim>& u)
        {
            double result = Delta<i,p>::delta*Delta<j,q>::delta
                + Delta<j,p>::delta*Delta<i,q>::delta;

            result += Delta<i,q>::delta*u[p][j] + u[p][i]*Delta<j,q>::delta;
            return 0.5*result;
        }

        template <int i, int j, int p, int q, int r, int s>
        static inline double E_dudu()
        {
            return 0.5 * Delta<p,r>::delta
                * ( Delta<i,q>::delta*Delta<j,s>::delta + Delta<i,s>::delta*Delta<j,q>::delta );
        }

        /** \brief Computes the value of det(I+u')
            \param u The deformation gradient: \f$\partial u_i / \partial x_j \f$
        */
        inline static double det_val(const Dune::FieldMatrix<double,3,3>& u) {
            return (1+u[0][0])*(1+u[1][1])*(1+u[2][2])
                + u[0][1]*u[1][2]*u[2][0]
                + u[0][2]*u[1][0]*u[2][1]
                - u[2][0]*(1+u[1][1])*u[0][2]
                - u[2][1]*u[1][2]*(1+u[0][0])
                - (1+u[2][2])*u[1][0]*u[0][1];
        }

        inline static double det_val(const Dune::FieldMatrix<double,2,2>& u) {
            return (1+u[0][0])*(1+u[1][1]) - u[1][0]*u[0][1];
        }



        // \parder {det I + u}{ u_pq }
        template <int p, int qq>
        static double det_du_tmpl(const Dune::FieldMatrix<double,3,3>& u) {
            int const q = qq+3;

            return (Delta<(p+1)%3,(q+1)%3>::delta + u[(p+1)%3][(q+1)%3])
                * (Delta<(p+2)%3,(q+2)%3>::delta+u[(p+2)%3][(q+2)%3])
                - (Delta<(p+1)%3,(q-1)%3>::delta+u[(p+1)%3][(q-1)%3])
                * (Delta<(p+2)%3,(q-2)%3>::delta+u[(p+2)%3][(q-2)%3]);
        }

        /** \brief Compute linearization of the determinant of a deformation gradient. Multiplication with a test function(scalar) gradient, gives the linearization in the direction of the test function.
         *
         *  \param u The displacement gradient(!) at which the determinant is evaluated
         */
        template <class field_type = double>
        void linearisedDefDet(const FieldMatrix<field_type, 3, 3>& u,
                              FieldMatrix<field_type, 3, 3>& linDet) {

            linDet = 0;
            for (int i=0; i<2; i++)
                for (int j=i+1; j<3; j++) {
                    int k=(3-(i+j))%3;
                    linDet[i][j] = u[j][k]*u[k][i] - u[j][i]*(1+u[k][k]);
                    linDet[j][i] = u[k][j]*u[i][k] - u[i][j]*(1+u[k][k]);
            }

            // the diagonal parts
            for (int i=0; i<3; i++)
                linDet[i][i] = (u[(i+1)%3][(i+1)%3]+1)*(u[(i+2)%3][(i+2)%3]+1)-u[(i+1)%3][(i+2)%3]*u[(i+2)%3][(i+1)%3];
        }

        /** \brief Compute linearization of the determinant of a deformation gradient. Multiplication with a test function(scalar) gradient, gives the linearization in the direction of the test function.
         *
         *  \param u The displacement gradient(!) at which the determinant is evaluated
         */
        template <class field_type = double>
        auto linearisedDefDet(const FieldMatrix<field_type, 3, 3>& u) {
            FieldMatrix<field_type, 3, 3> linDet(0);
            for (int i=0; i < 2; i++)
                for (int j=i+1; j < 3; j++) {
                    int k=(3 - (i+j)) % 3;
                    linDet[i][j] = u[j][k]*u[k][i] - u[j][i]*(1 + u[k][k]);
                    linDet[j][i] = u[k][j]*u[i][k] - u[i][j]*(1 + u[k][k]);
            }

            // the diagonal parts
            for (int i=0; i < 3; i++)
                linDet[i][i] = (u[(i+1)%3][(i+1)%3]+1)*(u[(i+2)%3][(i+2)%3]+1)
                            - u[(i+1)%3][(i+2)%3]*u[(i+2)%3][(i+1)%3];
            return linDet;
        }


        /** \brief Compute linearization of the determinant of a deformation gradient. Multiplication with a test function(scalar) gradient, gives the linearization in the direction of the test function.
         *
         *  \param u The displacement gradient(!) at which the determinant is evaluated
         */
        template <class field_type = double>
        void linearisedDefDet(const FieldMatrix<field_type, 2, 2>& u,
                              FieldMatrix<field_type, 2, 2>& linDet) {

            linDet = 0;
            for (int i=0; i<2; i++) {
                linDet[i][i] = 1+u[(i+1)%2][(i+1)%2];
                linDet[i][(i+1)%2] = -u[(i+1)%2][i];
            }
        }

        /** \brief Compute linearization of the determinant of a deformation gradient. Multiplication with a test function(scalar) gradient, gives the linearization in the direction of the test function.
         *
         *  \param u The displacement gradient(!) at which the determinant is evaluated
         */
        template<class field_type = double>
        auto linearisedDefDet(const FieldMatrix<field_type, 2, 2>& u) {

            FieldMatrix<field_type, 2, 2> linDet(0);
            for (int i=0; i<2; i++) {
                linDet[i][i] = 1 + u[(i+1) % 2][(i+1) % 2];
                linDet[i][(i+1) % 2] = -u[(i+1) % 2][i];
            }
            return linDet;
        }



        // \parder {det I + u}{ u_pq }
        template <int p, int q>
        static double det_du_tmpl(const FieldMatrix<double,2,2>& u) {
            return Delta<p,q>::delta * (1+u[(p+1)%2][(q+1)%2])
                - Delta<p,(q+1)%2>::delta * u[(p+1)%2][(q+1)%2];

            // p = 0, q = 0;  (1+u[1][1])
            // p = 0, q = 1; -u[1][0]
            // p = 1, q = 0; -u[0][1]
            // p = 1, q = 1;  (1+u[0][0])
            //
        }

        template <int p, int qq, int r, int s>
        static double det_dudu_tmpl(const FieldMatrix<double,3,3>& u) {
            int const q = qq+3;

            return
                  Delta<(p+1)%3,r>::delta*Delta<(q+1)%3,s>::delta * (Delta<(p+2)%3,(q+2)%3>::delta+u[(p+2)%3][(q+2)%3])
                + Delta<(p+2)%3,r>::delta*Delta<(q+2)%3,s>::delta * (Delta<(p+1)%3,(q+1)%3>::delta+u[(p+1)%3][(q+1)%3])
                - Delta<(p+1)%3,r>::delta*Delta<(q-1)%3,s>::delta * (Delta<(p+2)%3,(q-2)%3>::delta+u[(p+2)%3][(q-2)%3])
                - Delta<(p+2)%3,r>::delta*Delta<(q-2)%3,s>::delta * (Delta<(p+1)%3,(q-1)%3>::delta+u[(p+1)%3][(q-1)%3]);
        }

        // parder det (I + u) partial u_pq \partial u_rs
        template <int p, int q, int r, int s>
        static double det_dudu_tmpl(const FieldMatrix<double,2,2>& u) {
            return Delta<p,q>::delta*Delta<r,(p+1)%2>::delta*Delta<s,(q+1)%2>::delta
                - Delta<p,(q+1)%2>::delta*Delta<r,(p+1)%2>::delta*Delta<s,(q+1)%2>::delta;

            // 0 0 0 0   -->  0  - 0
            // 0 0 0 1   -->  0  - 0
            // 0 0 1 0   -->  0  - 0
            // 0 0 1 1   -->  1  - 0  = 1
            // 0 1 0 0   -->  0  - 0
            // 0 1 0 1   -->  0  - 0
            // 0 1 1 0   -->  0  - 1  = -1
            // 0 1 1 1   -->  0  - 0
            // 1 0 0 0   -->  0  - 0
            // 1 0 0 1   -->  0  - 1  = -1
            // 1 0 1 0   -->  0  - 0
            // 1 0 1 1   -->  0  - 0
            // 1 1 0 0   -->  1  - 0  = 1
            // 1 1 0 1   -->  0  - 0
            // 1 1 1 0   -->  0  - 0
            // 1 1 1 1   -->  0  - 0
        }

    }
}

#endif
