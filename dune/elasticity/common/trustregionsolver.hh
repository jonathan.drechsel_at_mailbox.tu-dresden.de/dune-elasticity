#ifndef DUNE_ELASTICITY_COMMON_TRUSTREGIONSOLVER_HH
#define DUNE_ELASTICITY_COMMON_TRUSTREGIONSOLVER_HH

#include <memory>
#include <vector>
#include <type_traits>

#include <dune/common/bitsetvector.hh>
#include <dune/common/concept.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/norms/h1seminorm.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/solvers/loopsolver.hh>

#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/p2nodalbasis.hh>
#include <dune/fufem/functionspacebases/p3nodalbasis.hh>
#include <dune/fufem/assemblers/transferoperatorassembler.hh>

#include <dune/functions/functionspacebases/concepts.hh>

#include <dune/elasticity/assemblers/feassembler.hh>

#if HAVE_DUNE_PARMG
#include <dune/parmg/iterationstep/lambdastep.hh>
#include <dune/parmg/iterationstep/multigrid.hh>
#include <dune/parmg/parallel/dofmap.hh>
#include <dune/parmg/iterationstep/multigridsetup.hh>
#include <dune/parmg/iterationstep/parallelprojectedgs.hh>
#include <dune/parmg/norms/normadapter.hh>
#include <dune/parmg/parallel/collectdiagonal.hh>
#include <dune/parmg/parallel/parallelenergyfunctional.hh>
#include <dune/parmg/parallel/parallelenergynorm.hh>
#include <dune/parmg/parallel/restrictmatrix.hh>
#endif


/** \brief Trust-region solver */
template <class BasisType, class VectorType>
class TrustRegionSolver
    : public IterativeSolver<VectorType,
                             Dune::BitSetVector<VectorType::value_type::dimension> >
{
    typedef typename BasisType::GridView::Grid GridType;
    using GridView = typename BasisType::GridView;

    const static int blocksize = VectorType::value_type::dimension;

    const static int gridDim = GridType::dimension;

    // Centralize the field type here
    typedef double field_type;

    // Some types that I need
    typedef Dune::BCRSMatrix<Dune::FieldMatrix<field_type, blocksize, blocksize> > MatrixType;
    typedef Dune::BlockVector<Dune::FieldVector<field_type, blocksize> >           CorrectionType;
    typedef VectorType                                                             SolutionType;

#if HAVE_DUNE_PARMG
    using MGSetup = Dune::ParMG::ParallelMultiGridSetup<BasisType, MatrixType, VectorType>;
#endif
public:

    TrustRegionSolver()
        : IterativeSolver<VectorType, Dune::BitSetVector<blocksize> >(0,100,NumProc::FULL),
          hessianMatrix_(std::shared_ptr<MatrixType>(NULL)), h1SemiNorm_(NULL)
    {
#if HAVE_DUNE_PARMG
      // TODO currently, we cannot use PARMG together with dune-functions bases
      static_assert( not Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>() ,
        "Currently, dune-parmg and dune-functions bases cannot be used together."
      );
#endif
    }

    /** \brief Set up the solver using a monotone multigrid method as the inner solver */
    void setup(const GridType& grid,
               std::conditional_t< // do we have a dune-functions basis? -> choose right assembler type
                  Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>(),
                  const Dune::Elasticity::FEAssembler<BasisType,VectorType>*,
                  const Dune::FEAssembler<DuneFunctionsBasis<BasisType>,VectorType>*> assembler,
               const SolutionType& x,
               const Dune::BitSetVector<blocksize>& dirichletNodes,
               double tolerance,
               int maxTrustRegionSteps,
               double initialTrustRegionRadius,
               int multigridIterations,
               double mgTolerance,
               int mu,
               int nu1,
               int nu2,
               int baseIterations,
               double baseTolerance,
               double damping);

    void setIgnoreNodes(const Dune::BitSetVector<blocksize>& ignoreNodes)
    {
        ignoreNodes_ = &ignoreNodes;
#if !HAVE_DUNE_PARMG
        std::shared_ptr<LoopSolver<CorrectionType> > loopSolver = std::dynamic_pointer_cast<LoopSolver<CorrectionType> >(innerSolver_);
        assert(loopSolver);
        loopSolver->iterationStep_->ignoreNodes_ = ignoreNodes_;
#endif
    }

    void solve();

    void setInitialIterate(const SolutionType& x) {
        x_ = x;
    }

    SolutionType getSol() const {return x_;}

protected:

    /** \brief The grid */
    const GridType* grid_;

    /** \brief The solution vector */
    SolutionType x_;

    /** \brief The initial trust-region radius in the maximum-norm */
    double initialTrustRegionRadius_;

    /** \brief Maximum number of trust-region steps */
    int maxTrustRegionSteps_;

    /** \brief Maximum number of multigrid iterations */
    int innerIterations_;

    /** \brief Error tolerance of the multigrid QP solver */
    double innerTolerance_;

    /** \brief Damping for the smoothers in the multigrid QP solver */
    double damping_;

    /** \brief Maximum number of base solver iterations */
    int baseIterations_;

    /** \brief Error tolerance of the base solver inside the multigrid QP solver */
    double baseTolerance_;

    /** \brief Hessian matrix */
    std::shared_ptr<MatrixType> hessianMatrix_;

    /** \brief The assembler for the material law */
    std::conditional_t< // do we have a dune-functions basis? -> choose right assembler type
      Dune::models< Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>(),
        const Dune::Elasticity::FEAssembler<BasisType,VectorType>*,
        const Dune::FEAssembler<DuneFunctionsBasis<BasisType>,VectorType>* > assembler_;

#if HAVE_DUNE_PARMG
    /** \brief The solver for the inner problem */
    std::unique_ptr<MGSetup> mgSetup_;

    /** \brief Iteration step for the quadratic inner problems */
    std::unique_ptr<Dune::ParMG::Multigrid<VectorType> > innerMultigridStep_;
#endif
    /** \brief The solver for the quadratic inner problems */
    std::shared_ptr<Solver> innerSolver_;

#if ! HAVE_DUNE_PARMG
    /** \brief Contains 'true' everywhere -- the trust-region is bounded */
    Dune::BitSetVector<blocksize> hasObstacle_;
#endif

    /** \brief The Dirichlet nodes */
    std::shared_ptr<const Dune::BitSetVector<blocksize> > ignoreNodes_;

    /** \brief The norm used to measure multigrid convergence */
    std::shared_ptr< H1SemiNorm<CorrectionType> > h1SemiNorm_;

    /** \brief An L2-norm, really.  The H1SemiNorm class is badly named */
    std::shared_ptr<H1SemiNorm<CorrectionType> > l2Norm_;

};

#include "trustregionsolver.cc"

#endif
