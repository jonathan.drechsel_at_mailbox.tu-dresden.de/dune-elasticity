#include <config.h>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/amirameshreader.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/io.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functions/coarsegridfunctionwrapper.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/sampleonbitfield.hh>
#include <dune/fufem/assemblers/localassemblers/neumannboundaryassembler.hh>
#include <dune/fufem/assemblers/boundaryfunctionalassembler.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>
#include <dune/fufem/estimators/hierarchicalestimator.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>

#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>
#include <dune/solvers/solvers/loopsolver.hh>

#include <dune/solvers/norms/energynorm.hh>



#define IPOPT_BASE

// The grid dimension
const int dim = 3;

using namespace Dune;

int main (int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);
    // Some types that I need
    typedef BCRSMatrix<FieldMatrix<double, dim, dim> > OperatorType;
    typedef BlockVector<FieldVector<double, dim> >     VectorType;

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("linelast.parset", parameterSet);

    // read solver settings
    const int minLevel         = parameterSet.get<int>("minLevel");
    const int maxLevel         = parameterSet.get<int>("maxLevel");
    const int numIt            = parameterSet.get<int>("numIt");
    const int nu1              = parameterSet.get<int>("nu1");
    const int nu2              = parameterSet.get<int>("nu2");
    const int mu               = parameterSet.get<int>("mu");
    const double tolerance     = parameterSet.get<double>("tolerance");
    const double baseTolerance = parameterSet.get<double>("baseTolerance");
    const bool paramBoundaries = parameterSet.get<int>("paramBoundaries");


    // read problem settings
    std::string path                = parameterSet.get<std::string>("path");
    std::string gridFile            = parameterSet.get<std::string>("gridFile");
    std::string parFile             = parameterSet.get("parFile", "");
    std::string dirichletFile       = parameterSet.get<std::string>("dirichletFile");
    std::string dirichletValuesFile = parameterSet.get<std::string>("dirichletValuesFile");
    std::string neumannFile       = parameterSet.get<std::string>("neumannFile");
    std::string neumannValuesFile = parameterSet.get<std::string>("neumannValuesFile");
    const double E                = parameterSet.get<double>("E",17e6);
    const double nu               = parameterSet.get<double>("nu",0.3);

    // /////////////////////////////
    //   Generate the grid
    // /////////////////////////////
    typedef UGGrid<dim> GridType;
    typedef BoundaryPatch<GridType::LevelGridView> LevelBoundaryPatch;
    typedef BoundaryPatch<GridType::LeafGridView> LeafBoundaryPatch;

    GridType* grid= new GridType;
    using GridView = GridType::LeafGridView;
    grid->setRefinementType(GridType::COPY);

#if HAVE_AMIRAMESH
    if (paramBoundaries)
        grid = AmiraMeshReader<GridType>::read(path + gridFile, PSurfaceBoundary<dim-1>::read(path + parFile));
    else
        grid = AmiraMeshReader<GridType>::read(path + gridFile);
#else
#warning You need libamiramesh for this code!
#endif

    LevelBoundaryPatch coarseDirichletBoundary;
    coarseDirichletBoundary.setup(grid->levelGridView(0));
    readBoundaryPatch<GridType>(coarseDirichletBoundary, path + dirichletFile);

    VectorType coarseDirichletValues(grid->size(0, dim));
#if HAVE_AMIRAMESH
    AmiraMeshReader<GridType>::readFunction(coarseDirichletValues, path + dirichletValuesFile);
#else
#warning You need libamiramesh for this code!
#endif

    LevelBoundaryPatch coarseNeumannBoundary;
    coarseNeumannBoundary.setup(grid->levelGridView(0));
    readBoundaryPatch<GridType>(coarseNeumannBoundary, path + neumannFile);

    VectorType coarseNeumannValues(grid->size(0, dim));
#if HAVE_AMIRAMESH
    AmiraMeshReader<GridType>::readFunction(coarseNeumannValues, path + neumannValuesFile);
#else
#warning You need libamiramesh for this code!
#endif
    P1NodalBasis<GridType::LevelGridView> coarseBasis(grid->levelGridView(0));

    auto coarseDir = ::Functions::makeFunction(coarseBasis,coarseDirichletValues);
    auto coarseNeumann = ::Functions::makeFunction(coarseBasis,coarseNeumannValues);

    for (int i=0; i<minLevel; i++)
        grid->globalRefine(1);

    using P1Basis = Dune::Functions::LagrangeBasis<GridView,1>;
    P1Basis p1Basis(grid->leafGridView());
    using FufemP1Basis = DuneFunctionsBasis<P1Basis>;
    FufemP1Basis p1NodalBasis(p1Basis);

    // initial iterate
    VectorType x(p1Basis.size());
    x = 0;

    // //////////////////////////////////////////////////
    //   Refinement loop
    // //////////////////////////////////////////////////

    while (true) {

        // Determine fine Dirichlet and Neumann dofs
        LeafBoundaryPatch leafDirichletBoundary(grid->leafGridView());
        BoundaryPatchProlongator<GridType>::prolong(coarseDirichletBoundary, leafDirichletBoundary);

        LeafBoundaryPatch leafNeumannBoundary(grid->leafGridView());
        BoundaryPatchProlongator<GridType>::prolong(coarseNeumannBoundary, leafNeumannBoundary);

        BitSetVector<dim> dirichletNodes(grid->size(dim),false);
        for (int i=0; i<grid->size(dim);i++)
            if (leafDirichletBoundary.containsVertex(i))
                for (int j=0; j<dim; j++)
                    dirichletNodes[i][j]=true;

        VectorType dirichletValues, neumannValues;

        CoarseGridFunctionWrapper<BasisGridFunction<P1NodalBasis<GridType::LevelGridView>, VectorType> > wrappedDir(coarseDir);
        ::Functions::interpolate(p1NodalBasis, dirichletValues, wrappedDir);

        CoarseGridFunctionWrapper<BasisGridFunction<P1NodalBasis<GridType::LevelGridView>, VectorType> > wrappedNeu(coarseNeumann);
        ::Functions::interpolate(p1NodalBasis, neumannValues, wrappedNeu);

        VectorType rhs(grid->size(dim));
        rhs = 0;

        for (size_t i=0; i<rhs.size(); i++)
            for (int j=0; j<dim; j++) {
                if (dirichletNodes[i][j])
                    x[i][j] = dirichletValues[i][j];

            }
        // Assemble elasticity problem#
        OperatorAssembler<FufemP1Basis,FufemP1Basis> p1Assembler(p1NodalBasis, p1NodalBasis);

        StVenantKirchhoffAssembler<GridType, FufemP1Basis::LocalFiniteElement, FufemP1Basis::LocalFiniteElement> p1LocalAssembler(E, nu);
        OperatorType stiffnessMatrix;

        p1Assembler.assemble(p1LocalAssembler, stiffnessMatrix);

        // Assemble Neumann forces
        BoundaryFunctionalAssembler<FufemP1Basis> boundaryFunctionalAssembler(p1NodalBasis,leafNeumannBoundary);
        BasisGridFunction<FufemP1Basis,VectorType> neumannFunction(p1NodalBasis, neumannValues);

        NeumannBoundaryAssembler<GridType, Dune::FieldVector<double,3> > localNeumannAssembler(neumannFunction);
        boundaryFunctionalAssembler.assemble(localNeumannAssembler, rhs);

        // ///////////////////////////
        //   Create a solver
        // ///////////////////////////

        // First create a base solver
#ifdef IPOPT_BASE
#if !defined HAVE_IPOPT
#error You need to have IPOpt installed if you want to use it as the base solver!
#endif
        QuadraticIPOptSolver<OperatorType,VectorType> baseSolver(baseTolerance, 100, Solver::QUIET);

#else // Gauss-Seidel is the base solver

        auto baseSolverStep = Dune::Solvers::BlockGSStepFactory<OperatorType, VectorType>::create(
               Dune::Solvers::BlockGS::LocalSolvers::direct(0.0));


        EnergyNorm<OperatorType,VectorType> baseEnergyNorm(baseSolverStep);

        LoopSolver<VectorType> baseSolver(baseSolverStep,
                                               baseIt,
                                               baseTolerance,
                                               baseEnergyNorm,
                                               Solver::QUIET);
#endif

        // Make pre and postsmoothers
        auto presmoother = Dune::Solvers::BlockGSStepFactory<OperatorType, VectorType>::create(
               Dune::Solvers::BlockGS::LocalSolvers::direct(0.0));
        auto postsmoother = Dune::Solvers::BlockGSStepFactory<OperatorType, VectorType>::create(
               Dune::Solvers::BlockGS::LocalSolvers::direct(0.0));

        MultigridStep<OperatorType, VectorType> multigridStep(stiffnessMatrix, x, rhs);

        multigridStep.setMGType(mu, nu1, nu2);
        multigridStep.setIgnore(dirichletNodes);
        multigridStep.setBaseSolver(baseSolver);
        multigridStep.setSmoother(&presmoother,&postsmoother);

        std::vector<std::shared_ptr<CompressedMultigridTransfer<VectorType> > > mgTransfers(grid->maxLevel());
        for (size_t i=0; i<mgTransfers.size(); i++) {
            mgTransfers[i] = std::make_shared<CompressedMultigridTransfer<VectorType> >();
            mgTransfers[i]->setup(*grid, i, i+1);
        }

        multigridStep.setTransferOperators(mgTransfers);
        EnergyNorm<OperatorType, VectorType> energyNorm(multigridStep);

        LoopSolver<VectorType> solver(multigridStep,
                                      numIt,
                                      tolerance,
                                      energyNorm,
                                      Solver::FULL);



        solver.preprocess();

        // Compute solution
        solver.solve();

        x = multigridStep.getSol();

        // ////////////////////////////////////////////////////////////////////////
        //    Leave adaptation loop if maximum number of levels has been reached
        // ////////////////////////////////////////////////////////////////////////

        if (grid->maxLevel() == maxLevel)
            break;

        // /////////////////////////////////////////////////////////////
        //   Estimate error and refine grid
        // /////////////////////////////////////////////////////////////

        using P2Basis = Dune::Functions::LagrangeBasis<GridView,2>;
        using FufemP2NodalBasis = DuneFunctionsBasis<P2Basis>;
        HierarchicalEstimator<FufemP2NodalBasis,dim> estimator(*grid);

        RefinementIndicator<GridType> indicator(*grid);

        typedef FufemP2NodalBasis::LocalFiniteElement P2FiniteElement;
        StVenantKirchhoffAssembler<GridType,P2FiniteElement,P2FiniteElement>* localStiffness = new StVenantKirchhoffAssembler<GridType,P2FiniteElement,P2FiniteElement>(E,nu);


        estimator.estimate(x, nullptr, nullptr,
                leafDirichletBoundary, indicator,
                localStiffness);

        //estimator.markForRefinement(grid-> hierarchicError, threshold);
        FractionalMarkingStrategy<GridType>::mark(indicator, *grid, 0.2);
        // ////////////////////////////////////////////////////
        //   Refine grid
        // ////////////////////////////////////////////////////

        GridFunctionAdaptor<FufemP1Basis> adaptorP1(p1NodalBasis,true,true);

        grid->preAdapt();
        grid->adapt();
        grid->postAdapt();

        p1NodalBasis.update();
        adaptorP1.adapt(x);

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grid refined" << std::endl;
        std::cout << "  Level: " << grid->maxLevel()
                  << "   vertices: " << grid->size(grid->maxLevel(), dim)
                  << "   elements: " << grid->size(grid->maxLevel(), 0) << std::endl;
        std::cout << "########################################################" << std::endl;

    }

    // Output result
    auto displacementFunction = Dune::Functions::makeDiscreteGlobalBasisFunction<FieldVector<double,dim> >(p1Basis, x);

    VTKWriter<GridView> vtkWriter(grid->leafGridView());
    vtkWriter.addVertexData(displacementFunction, VTK::FieldInfo("displacement", VTK::FieldInfo::Type::vector, dim));
    vtkWriter.write("linear-elasticity-result");

} catch (Exception& e)
{
  std::cout << e.what() << std::endl;
}



